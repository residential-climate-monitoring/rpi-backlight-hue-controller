# rpi-backlight-hue-controller

Controls the Rpi Backlight based on Hue Motion Sensors.

## Getting started

- Clone project (assuming you're at `/home/pi`):

    ```shell
    git clone https://gitlab.com/residential-climate-monitoring/rpi-backlight-hue-controller.git
    ```

- Discover IP address of the bridge:

  Go to [discovery.meethue.com](https://discovery.meethue.com/) and write down the internal IP address.


- Create Philips Hue Bridge API user:

    See this [getting started guide](https://developers.meethue.com/develop/get-started-2/) and write down your username.


- Locate motion and light sensor:

    Go to http://YOUR_INTERNAL_IP/api/YOUR_USERNAME/sensors and write down the IDs of the sensors you wish to use.


- Configure env variables:

    ```shell
    sudo nano .env
    ```
    - Update `hue_base_uri` with your local IP address and username.
    - Update `hue_motion_sensor_id` and `hue_light_sensor_id` with the IDs of your choice.


- Create [virtual env](https://docs.python.org/3/tutorial/venv.html):

    ```shell
    python3 -m venv venv
    ```

- Activate virtual env:

    ```shell
    source venv/bin/activate
    ```

- Install dependencies:

    ```shell
    pip install -r requirements.txt
    ```

- Run the script and test it:

    ```shell
    python3 main.py
    ```

## Installing service

- Install as [systemd](https://www.thedigitalpictureframe.com/ultimate-guide-systemd-autostart-scripts-raspberry-pi/) service:

    ```shell
    sudo nano /etc/systemd/system/rpi-backlight-hue-controller.service
    ```

- Paste the following contents and save the file (`/etc/systemd/system/rpi-backlight-hue-controller.service`):

    ```shell
    [Unit]
    Description=Raspberry Pi Backlight Hue Controller
    After=multi-user.target
    Requires=network.target
    
    [Service]
    Type=idle
    User=ubuntu
    ExecStart=/home/pi/rpi-backlight-hue-controller/venv/bin/python3 /home/pi/rpi-backlight-hue-controller/main.py
    Restart=always
    RestartSec=60
    
    [Install]
    WantedBy=multi-user.target
    ```
    _Note: adjust the path of ExecStart accordingly to your system. E.g. "venv" refers to the name of the virtual env created earlier and the directory where the repo is checked out might also differ._


- Change file permissions

    ```shell
    sudo chmod 644 /etc/systemd/system/rpi-backlight-hue-controller.service
    ```

- Inform the system of the new service:

    ```shell
    sudo systemctl daemon-reload
    sudo systemctl enable rpi-backlight-hue-controller.service
    ```

- Start the service:

    ```shell
    sudo systemctl start rpi-backlight-hue-controller.service
    ```


# Troubleshooting

To troubleshoot, the logs can be found at: `/home/pi/rpi-backlight-hue-controller.log`


# License

The source code and all other files in this repository are licensed under the MIT
license, so you can easily use it in your own projects. See [`LICENSE`](LICENSE) for


# Credits

Special thanks to Linus Groh for creating the [RPI backlight library](https://github.com/linusg/rpi-backlight) that this library depends upon.