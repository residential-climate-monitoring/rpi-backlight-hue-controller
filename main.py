#  Copyright (c) 2021 Pim Hazebroek
#  This program is made available under the terms of the MIT License.
#  For full details check the LICENSE file at the root of the project.
import logging
import os
import time
from logging.handlers import RotatingFileHandler

import requests
from dotenv import load_dotenv
from rpi_backlight import Backlight

ONE_MB = '1048576'


def main():
    if not _is_presence():
        if backlight.power:
            logging.info('Turning backlight off due to lack of motion')
            backlight.power = False
        return
    brightness = _get_brightness()
    logging.info(f'Turning backlight on with brightness to: {brightness}')
    backlight.brightness = brightness
    backlight.power = True
    time.sleep(int(os.getenv('rpi_backlight_timeout', 300)))


def _is_presence():
    """Checks whether the motion sensor has picked up any presence"""
    state = _get_sensor_state(os.getenv('hue_motion_sensor_id'))
    presence = state['presence']
    return presence


def _get_brightness():
    """Returns the level of light"""
    state = _get_sensor_state(os.getenv('hue_light_sensor_id'))
    if state['dark'] and not state['daylight']:
        return int(os.getenv('rpi_brightness_dark', 10))
    if not state['dark'] and not state['daylight']:
        return int(os.getenv('rpi_brightness_twilight', 20))
    return int(os.getenv('rpi_brightness_daylight', 50))


def _get_sensor_state(sensor_id):
    """Retrieve the state for a specific sensor"""
    hue_base_uri = os.getenv('hue_base_uri')
    try:
        uri = f'{hue_base_uri}/sensors/{sensor_id}'
        response = requests.get(uri)
        return response.json()['state']
    except Exception as ex:
        logging.exception(f'Failed to retrieve state of sensor [{id}]', exc_info=ex)


def _init_logger():
    log_file = os.getenv('log-file', '/home/pi/rpi-backlight-hue-controller.log')
    log_level = os.getenv('log-level', 'WARN')
    log_max_bytes = int(os.getenv('log-max-bytes', ONE_MB))
    log_backup_count = int(os.getenv('log-backup-count', '0'))
    formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(name)s - %(message)s')
    rotating_file_handler = RotatingFileHandler(log_file, maxBytes=log_max_bytes, backupCount=log_backup_count)
    rotating_file_handler.setFormatter(formatter)
    logger = logging.getLogger()
    logger.addHandler(rotating_file_handler)
    logger.setLevel(log_level)


if __name__ == '__main__':
    load_dotenv()
    _init_logger()
    """Execute the script when run stand-alone"""
    backlight = Backlight()
    logging.info('Rpi Backlight Hue Controller running')
    while True:
        main()
        time.sleep(2)
